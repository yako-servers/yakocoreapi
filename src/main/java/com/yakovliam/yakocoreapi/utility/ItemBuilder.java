package com.yakovliam.yakocoreapi.utility;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;
import java.util.stream.Collectors;

public class ItemBuilder implements Cloneable {
    private Material material;
    private int amount = 1;
    private byte data;
    private String displayName;
    private List<String> lore = Lists.newArrayList();
    private Set<ItemFlag> itemFlags = Sets.newHashSet();
    private Map<Enchantment, Integer> enchants = Maps.newHashMap();
    private Color colour;
    private String skinData;
    private String owner;

    public ItemBuilder(ItemStack item) {
        this.material = item.getType();
        this.amount = item.getAmount();
        this.data = item.getData().getData();
        ItemMeta meta = item.getItemMeta();
        this.itemFlags = Sets.newHashSet(meta.getItemFlags());
        if (meta.hasDisplayName()) {
            this.displayName = meta.getDisplayName();
        }

        if (meta.hasLore()) {
            this.lore = Lists.newArrayList(meta.getLore());
        }

        if (meta.hasEnchants()) {
            this.enchants = Maps.newHashMap(meta.getEnchants());
        }

        if (item.getItemMeta() instanceof LeatherArmorMeta) {
            this.colour = ((LeatherArmorMeta) item.getItemMeta()).getColor();
        }

        if (item.getItemMeta() instanceof SkullMeta && ((SkullMeta) item.getItemMeta()).hasOwner()) {
            this.owner = ((SkullMeta) item.getItemMeta()).getOwner();
        }

    }

    public ItemBuilder(Material material, byte data, int amount, String displayName) {
        this.material = material;
        this.data = data;
        this.amount = amount;
        this.displayName = displayName;
    }

    public ItemBuilder(Material material, byte data, int amount) {
        this.material = material;
        this.data = data;
        this.amount = amount;
    }

    public ItemBuilder(Material material, byte data) {
        this.material = material;
        this.data = data;
    }

    public ItemBuilder(Material material) {
        this.material = material;
    }

    public ItemBuilder(ConfigurationSection section) {
        try {
            this.setMaterial(Material.valueOf(section.getString("material").toUpperCase()));
        } catch (IllegalArgumentException var7) {
            var7.printStackTrace();
            this.material = Material.AIR;
            System.err.println("Invalid material type");
        }

        this.setData(section.contains("data") ? (byte) section.getInt("data") : 0);
        this.setAmount(section.contains("amount") ? section.getInt("amount") : 1);
        if (section.contains("lore")) {
            this.setLore(section.getStringList("lore").stream().map((s) -> ChatColor.translateAlternateColorCodes('&', s)).collect(Collectors.toList()));
        }

        if (section.contains("displayName")) {
            this.setDisplayName(ChatColor.translateAlternateColorCodes('&', section.getString("displayName")));
        }

        if (section.contains("flags")) {
            try {
                this.setItemFlags(section.getStringList("flags").stream().map((s) -> ItemFlag.valueOf(s.toUpperCase())).collect(Collectors.toSet()));
            } catch (IllegalArgumentException var6) {
                var6.printStackTrace();
                System.err.println("Unknown item flag.");
            }
        }

        if (section.contains("enchantments")) {

            for (String type : section.getConfigurationSection("enchantments").getKeys(false)) {
                Enchantment enchantment = Enchantment.getByName(type.toUpperCase());
                if (enchantment == null) {
                    System.err.println("Unknown enchantment: " + type);
                } else {
                    this.addEnchantment(enchantment, section.getInt("enchantments." + type));
                }
            }
        }

        if (section.contains("r") && section.contains("g") && section.contains("b")) {
            this.colour = Color.fromRGB(section.getInt("r"), section.getInt("g"), section.getInt("b"));
        }

        if (section.contains("skinData")) {
            this.skinData = section.getString("skinData");
        }

        if (section.contains("owner")) {
            this.owner = section.getString("owner");
        }

    }

    public ItemBuilder(JsonObject object) {
        try {
            this.setMaterial(Material.valueOf(object.get("material").getAsString().toUpperCase()));
        } catch (IllegalArgumentException var7) {
            var7.printStackTrace();
            this.material = Material.AIR;
            System.err.println("Invalid material type");
        }

        this.setData(object.has("data") ? object.get("data").getAsByte() : 0);
        this.setAmount(object.has("amount") ? object.get("amount").getAsInt() : 1);
        JsonArray array;
        int i;
        if (object.has("lore")) {
            array = object.get("lore").getAsJsonArray();

            for (i = 0; i < array.size(); ++i) {
                this.addToLore(ChatColor.translateAlternateColorCodes('&', array.get(i).getAsString()));
            }
        }

        if (object.has("displayName")) {
            this.setDisplayName(ChatColor.translateAlternateColorCodes('&', object.get("displayName").getAsString()));
        }

        if (object.has("flags")) {
            try {
                array = object.get("flags").getAsJsonArray();

                for (i = 0; i < array.size(); ++i) {
                    this.addItemFlags(ItemFlag.valueOf(array.get(i).getAsString().toUpperCase()));
                }
            } catch (IllegalArgumentException var8) {
                var8.printStackTrace();
                System.err.println("Unknown item flag.");
            }
        }

        if (object.has("enchantments")) {
            array = object.get("enchantments").getAsJsonArray();

            for (i = 0; i < array.size(); ++i) {
                JsonObject enchantment = array.get(i).getAsJsonObject();
                Enchantment type = Enchantment.getByName(enchantment.get("type").getAsString());
                if (type == null) {
                    System.err.println("Unknown enchantment: " + type);
                } else {
                    this.addEnchantment(type, enchantment.get("level").getAsInt());
                }
            }
        }

        if (object.has("r") && object.has("g") && object.has("b")) {
            this.colour = Color.fromRGB(object.get("r").getAsInt(), object.get("g").getAsInt(), object.get("b").getAsInt());
        }

        if (object.has("skinData")) {
            this.skinData = object.get("skinData").getAsString();
        }

        if (object.has("owner")) {
            this.owner = object.get("owner").getAsString();
        }

    }

    public ItemBuilder setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public ItemBuilder setData(byte data) {
        this.data = data;
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public ItemBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemBuilder setLore(String... lore) {
        this.lore = Lists.newArrayList(lore);
        return this;
    }

    public ItemBuilder addToLore(String... lore) {
        this.lore.addAll(Arrays.asList(lore));
        return this;
    }

    public ItemBuilder setItemFlags(Set<ItemFlag> itemFlags) {
        this.itemFlags = itemFlags;
        return this;
    }

    public ItemBuilder setItemFlags(ItemFlag... itemFlags) {
        this.itemFlags = Sets.newHashSet(itemFlags);
        return this;
    }

    public ItemBuilder addItemFlags(ItemFlag... itemFlags) {
        this.itemFlags.addAll(Sets.newHashSet(itemFlags));
        return this;
    }

    public ItemBuilder setEnchants(Map<Enchantment, Integer> enchants) {
        this.enchants = enchants;
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
        this.enchants.put(enchantment, level);
        return this;
    }

    public ItemStack build() {
        ItemStack item = new ItemStack(this.material, this.amount, this.data);
        ItemMeta meta = item.getItemMeta();
        if (this.displayName != null) {
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', this.displayName));
        }

        List<String> newLore = new ArrayList<>();
        if (!this.lore.isEmpty()) {

            for (String s : this.lore) {
                newLore.add(ChatColor.translateAlternateColorCodes('&', s));
            }

            meta.setLore(newLore);
        }

        if (!this.itemFlags.isEmpty()) {
            meta.addItemFlags(this.itemFlags.toArray(new ItemFlag[0]));
        }

        if (!this.enchants.isEmpty()) {
            this.enchants.forEach((enchantment, level) -> meta.addEnchant(enchantment, level, true));
        }

        if (meta instanceof LeatherArmorMeta) {
            ((LeatherArmorMeta) meta).setColor(this.colour);
        } else if (meta instanceof SkullMeta) {
            SkullMeta skullMeta = (SkullMeta) meta;
            if (this.owner != null) {
                skullMeta.setOwner(this.owner);
            }
        }

        item.setItemMeta(meta);
        return item;
    }

    public void saveTo(ConfigurationSection section) {
        if (section.contains("enchantments")) {

            for (String type : section.getConfigurationSection("enchantments").getKeys(false)) {
                Enchantment enchantment = Enchantment.getByName(type.toUpperCase());
                if (enchantment == null) {
                    System.err.println("Unknown enchantment: " + type);
                } else {
                    this.addEnchantment(enchantment, section.getInt("enchantments." + type));
                }
            }
        }

        section.set("material", this.material.name());
        section.set("data", this.data);
        section.set("amount", this.amount);
        if (this.lore != null && !this.lore.isEmpty()) {
            section.set("lore", this.lore);
        }

        if (this.displayName != null) {
            section.set("displayName", this.displayName);
        }

        if (this.itemFlags != null && !this.itemFlags.isEmpty()) {
            section.set("flags", this.itemFlags.stream().map(Enum::name).collect(Collectors.toList()));
        }

        if (this.enchants != null && !this.enchants.isEmpty()) {
            this.enchants.forEach((enchantmentx, level) -> section.set("enchantments." + enchantmentx.getName(), level));
        }

        if (this.skinData != null) {
            section.set("skinData", this.skinData);
        }

        if (this.owner != null) {
            section.set("owner", this.owner);
        }

    }

    public ItemBuilder clone() {
        ItemBuilder builderNew = new ItemBuilder(this.material, this.data, this.amount, this.displayName);
        builderNew.owner = this.owner;
        builderNew.skinData = this.skinData;
        if (this.colour != null) {
            builderNew.colour = Color.fromRGB(this.colour.getRed(), this.colour.getGreen(), this.colour.getBlue());
        }

        builderNew.enchants = Maps.newHashMap(this.enchants);
        builderNew.lore = Lists.newArrayList(this.lore);
        builderNew.itemFlags = Sets.newHashSet(this.itemFlags);
        return builderNew;
    }
}
