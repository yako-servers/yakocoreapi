package com.yakovliam.yakocoreapi.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiConsumer;

public class ChatListener implements Listener {

    private static Map<UUID, BiConsumer<Player, String>> listenForInput = new HashMap<>();

    public static void listen(UUID uuid, BiConsumer<Player, String> consumer) {
        listenForInput.put(uuid, consumer);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        if (listenForInput.containsKey(uuid)) {
            try {
                event.setCancelled(true);
                listenForInput.get(uuid).accept(player, ChatColor.stripColor(event.getMessage()));
            } catch (Exception ignored) {} finally {
                listenForInput.remove(uuid);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        if (listenForInput.containsKey(uuid)) {
            try {
                event.setCancelled(true);
                listenForInput.get(uuid).accept(player, ChatColor.stripColor(event.getMessage()));
            } catch (Exception ignored) {} finally {
                listenForInput.remove(uuid);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        listenForInput.remove(event.getPlayer().getUniqueId());
    }
}
