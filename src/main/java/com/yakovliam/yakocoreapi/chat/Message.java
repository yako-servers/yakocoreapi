package com.yakovliam.yakocoreapi.chat;

import com.yakovliam.yakocoreapi.Pair;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Message {

    private String ident;
    private String defaultMessage;
    private String message = null;
    private boolean isGlobal = false;

    public Message(String ident, String defaultMessage) {
        this.ident = ident;
        this.defaultMessage = defaultMessage;
    }

    public String getIdent() {
        return this.ident;
    }

    public String getMessage() {
        return this.message == null ? ChatColor.translateAlternateColorCodes('&', this.defaultMessage) : ChatColor.translateAlternateColorCodes('&', this.message);
    }

    public String getMessage(Player player) {
        return this.getMessage();
    }

    public void broadcast(String... replacers) {
        this.msg(new ArrayList<>(Bukkit.getOnlinePlayers()), replacers);
    }

    public void msg(CommandSender to, String... replacers) {
        this.msg(Collections.singletonList(to), replacers);
    }

    public void msg(Collection<CommandSender> to, String... replacers) {
        Set<Pair<String, String>> pairs = new HashSet<>();

        Pair<String, String> pair = new Pair<>(null, null);

        for (int x = 0; x < replacers.length; ++x) {

            if (x % 2 == 0) {
                pair.setA(replacers[x]);
            } else {
                pair.setB(replacers[x]);
                pairs.add(pair);
                pair = new Pair<>(null, null);
            }

        }

        String[] lines = this.getMessage().split("\n");

        for (String s : lines) {

            String line = s;

            Iterator<Pair<String, String>> charItr;
            Pair<String, String> repl;
            for (charItr = pairs.iterator(); charItr.hasNext(); line = line.replaceAll(Pattern.quote(repl.getA()), Matcher.quoteReplacement(repl.getB()))) {
                repl = charItr.next();
            }

            for (CommandSender p : to) {
                p.sendMessage(line);
            }
        }

    }

    public String toString() {
        return this.getMessage();
    }
}