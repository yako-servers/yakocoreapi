package com.yakovliam.yakocoreapi.chat;

public class Localization {

    public static Message PLAYERS_ONLY = new Message("players-only", "&cYou need to be a player to use this command!");
    public static Message ACCESS_DENIED = new Message("access-denied", "&cYou do not have permission.");

}
