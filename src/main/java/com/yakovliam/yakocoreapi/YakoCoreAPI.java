package com.yakovliam.yakocoreapi;

import com.yakovliam.yakocoreapi.chat.ChatListener;
import com.yakovliam.yakocoreapi.gui.listener.GuiClickListener;
import com.yakovliam.yakocoreapi.gui.listener.GuiCloseListener;
import com.yakovliam.yakocoreapi.gui.manager.GuisManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

public class YakoCoreAPI {

    private static YakoCoreAPI instance;
    private static Plugin contextPlugin;
    private GuisManager guisManager;

    public static YakoCoreAPI getInstance(Plugin plugin) {
        if (instance == null) {
            setContextPlugin(plugin);
            instance = new YakoCoreAPI();
        }
        return instance;
    }

    public YakoCoreAPI() {
        // chat listener
        getContextPlugin().getServer().getPluginManager().registerEvents(new ChatListener(), getContextPlugin());
        // gui
        getContextPlugin().getServer().getPluginManager().registerEvents(new GuiClickListener(), getContextPlugin());
        getContextPlugin().getServer().getPluginManager().registerEvents(new GuiCloseListener(),
                getContextPlugin());
    }

    public static Plugin getContextPlugin() {
        return contextPlugin;
    }

    public static void setContextPlugin(Plugin ctx) {
        contextPlugin = ctx;
    }

    public static void err(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + getContextPlugin().getDescription().getFullName() + " | " + ChatColor.RED + " Error > " + message);
    }
}
