package com.yakovliam.yakocoreapi.command;

import com.yakovliam.yakocoreapi.chat.Localization;
import com.yakovliam.yakocoreapi.command.addons.Permissible;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class YakoSubCommand {

    public List<String> aliases;
    private List<YakoSubCommand> subCommands;

    public YakoSubCommand() {
        this(Collections.emptyList());
    }

    public YakoSubCommand(List<String> aliases) {
        this.subCommands = new ArrayList<>();
        this.aliases = aliases;
    }

    public abstract void onCommandExecuted(CommandSender sender, String[] args);

    public String getPermission() {
        return this.getClass().isAnnotationPresent(Permissible.class) ? this.getClass().getAnnotation(Permissible.class).value() : null;
    }

    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) { // if the sender is NOT a player
            if (args.length <= 0) { // if there are no args
                this.onCommandExecuted(sender, args); // call onCommandExecuted
            } else {
                for (YakoSubCommand subCommand : this.subCommands) {
                    for (String aliases : subCommand.aliases) {
                        if (args[0].equalsIgnoreCase(aliases)) {
                            subCommand.execute(sender, args);
                            break; // no need to continue
                        }
                    }
                }
            }
        } else { // sender is a player
            Player player = (Player) sender;
            if (this.getClass().isAnnotationPresent(Permissible.class) && !player.hasPermission(this.getClass().getAnnotation(Permissible.class).value())) { // if the player doesn't have permission
                Localization.ACCESS_DENIED.msg(sender);
            } else if (args.length <= 0) { // they have permission and there are no args
                this.onCommandExecuted(sender, args);
            } else { // they have permission and there might be sub commands

                for (YakoSubCommand subCommand : this.subCommands) {
                    for (String aliases : subCommand.aliases) {
                        if (args[0].equalsIgnoreCase(aliases)) {
                            if (subCommand.getPermission() == null || sender.hasPermission(subCommand.getPermission())) {
                                subCommand.execute(sender, args);
                                break;
                            }
                            Localization.ACCESS_DENIED.msg(sender);
                        }
                    }
                }
            }

        }
    }

    public void addSubCommands(YakoSubCommand... commands) {
        this.subCommands.addAll(Arrays.asList(commands));
    }
}