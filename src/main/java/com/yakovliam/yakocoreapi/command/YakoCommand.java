package com.yakovliam.yakocoreapi.command;

import com.yakovliam.yakocoreapi.chat.Localization;
import com.yakovliam.yakocoreapi.command.addons.NoConsole;
import com.yakovliam.yakocoreapi.command.addons.Permissible;
import com.yakovliam.yakocoreapi.command.addons.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.*;

public abstract class YakoCommand extends BukkitCommand {

    private List<YakoCommand> subCommands;

    public YakoCommand(String name) {
        this(name, "", Collections.emptyList());
    }

    public YakoCommand(String name, String description) {
        this(name, description, Collections.emptyList());
    }

    public YakoCommand(String name, List<String> aliases) {
        this(name, "", aliases);
    }

    public YakoCommand(String name, String description, List<String> aliases) {
        super(name, description, "/" + name, aliases);
        this.subCommands = new ArrayList<>();

        if (!this.getClass().isAnnotationPresent(SubCommand.class)) {
            this.registerCommand();
        }
    }

    private void registerCommand() {
        try {

            Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap"); // get command map
            field.setAccessible(true);

            CommandMap commandMap = (CommandMap) field.get(Bukkit.getServer()); // assign command map

            if (this.getClass().isAnnotationPresent(Permissible.class)) { // if annotation for permissible is avail, set permission
                this.setPermission(this.getClass().getAnnotation(Permissible.class).value());
            }

            Command command = commandMap.getCommand(this.getName()); // create command

            if (command != null) {
                Map map;

                Field f = commandMap.getClass().getDeclaredField("knownCommands");
                f.setAccessible(true);
                map = (Map) f.get(commandMap);

                command.unregister(commandMap);
                map.remove(this.getName());
                this.getAliases().forEach(map::remove);
            }

            commandMap.register(this.getName(), this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {

        if (this.getClass().isAnnotationPresent(NoConsole.class) && !(sender instanceof Player)) { // if no console is present
            Localization.PLAYERS_ONLY.msg(sender);
            return true;
        } else if (this.getClass().isAnnotationPresent(Permissible.class) && !sender.hasPermission(this.getClass().getAnnotation(Permissible.class).value())) {
            Localization.ACCESS_DENIED.msg(sender);
            return true;
        } else if (args.length <= 0) {
            this.onCommand(sender, commandLabel, args);
            return true;
        } else {
            YakoCommand sub = null;

            for (YakoCommand subCommand : this.subCommands) { // loop through sub commands
                if (subCommand.getName().equalsIgnoreCase(args[0])) { // if the name is the current 1st argument
                    sub = subCommand; // set (execute later)
                    break;
                }

                for (String alias : subCommand.getAliases()) { // check aliases of the subCommand
                    if (alias.equalsIgnoreCase(args[0])) { // if the alias is the current 1st argument
                        sub = subCommand; // set (execute later)
                        break;
                    }
                }
            }

            if (sub != null) { // if there is a sub command

                if (sub.getPermission() != null && !sender.hasPermission(sub.getPermission())) { // if the user doesn't have permission for the sub command
                    Localization.ACCESS_DENIED.msg(sender);
                    return true;
                } else {
                    // yes, they have permission
                    String[] subArgs; // get arguments for the sub command (usually args[1...end], or 2nd arg to end)
                    if (args.length > 1) {
                        subArgs = Arrays.copyOfRange(args, 1, args.length); // ^
                    } else {
                        subArgs = new String[0];
                    }

                    sub.execute(sender, args[0], subArgs); // execute with those sub args
                    return true;
                }
            } else { // it's just regular...no sub command... execute regularly
                this.onCommand(sender, commandLabel, args);
                return true;
            }
        }
    }

    public abstract void onCommand(CommandSender sender, String[] args);

    public void onCommand(CommandSender sender, String label, String[] args) {
        this.onCommand(sender, args);
    }

    public void addSubCommands(YakoCommand... commands) {
        this.subCommands.addAll(Arrays.asList(commands));
    }

}
