package com.yakovliam.yakocoreapi.gui.listener;

import com.yakovliam.yakocoreapi.gui.DynGui;
import com.yakovliam.yakocoreapi.gui.manager.GuisManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class GuiCloseListener implements Listener {

    @EventHandler
    public void onGuiClose(InventoryCloseEvent event) {
        // check to see if this inventory is one of the guis
        boolean hasOpen = GuisManager.hasOpen(event.getPlayer().getUniqueId());

        if (hasOpen) {
            // check similarity
            DynGui openGui = GuisManager.get(event.getPlayer().getUniqueId());
            boolean same = event.getInventory().equals(openGui.getInventory());
//            boolean same = ChatColor.stripColor(event.getInventory().getTitle()).equals(ChatColor.stripColor(openGui.getDisplayName()));
            if (same) {
                GuisManager.close(event.getPlayer().getUniqueId());
            }
        }
    }
}
