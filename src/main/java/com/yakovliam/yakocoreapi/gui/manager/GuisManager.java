package com.yakovliam.yakocoreapi.gui.manager;

import com.yakovliam.yakocoreapi.YakoCoreAPI;
import com.yakovliam.yakocoreapi.gui.DynGui;
import com.yakovliam.yakocoreapi.gui.listener.GuiClickListener;
import com.yakovliam.yakocoreapi.gui.listener.GuiCloseListener;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GuisManager {

    private static Map<UUID, DynGui> openGuis = new HashMap<>();

    public static void add(UUID u, DynGui g) {
        openGuis.put(u, g);
    }

    public static void close(UUID u) {
        openGuis.remove(u);
    }

    public static boolean hasOpen(UUID u) {
        return openGuis.containsKey(u);
    }

    public static DynGui get(UUID u) {
        if (openGuis.containsKey(u)) return openGuis.get(u);
        return null;
    }
}
