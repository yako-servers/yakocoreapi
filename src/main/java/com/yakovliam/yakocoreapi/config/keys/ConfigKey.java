package com.yakovliam.yakocoreapi.config.keys;

import com.yakovliam.yakocoreapi.config.adapter.ConfigurationAdapter;

public interface ConfigKey<T> {

    // position in enum
    int ordinal();

    // returns the actual value
    T get(ConfigurationAdapter adapter);
}
